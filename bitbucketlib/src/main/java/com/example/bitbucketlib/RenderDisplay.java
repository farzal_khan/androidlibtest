package com.example.bitbucketlib;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class RenderDisplay extends AppCompatActivity {

    private WebView myWebView; /* Declaring a WebView variable. */
    private ProgressBar pBar;
    private boolean showLoader;
    private WebChromeClient webChromeClient;    // declaring WebChromeClient null object
    String postData = null;
    String email = "", first_name = "", phone_no = "", client_id = "";

    @SuppressLint({"SetJavaScriptEnabled", "AddJavascriptInterface"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_render_display);

        Intent i = getIntent();

        if (i.hasExtra("email") && i.hasExtra("first_name") && i.hasExtra("phone_no") && i.hasExtra("client_id")) {

            email = i.getStringExtra("email");
            first_name = i.getStringExtra("first_name");
            phone_no = i.getStringExtra("phone_no");
            client_id = i.getStringExtra("client_id");
            openWebView();
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void openWebView() {
        /* SDK WEBVIEW LOGIC GOES HERE*/
        showLoader = true;
        pBar = (ProgressBar) findViewById(R.id.pBar);
        myWebView = (WebView) findViewById(R.id.myWebView); /* Initializing WebView object. */
        webChromeClient = new WebChromeClient();    /* Initializing webChromeClient object */
        final WebAppInterface appInterface = WebAppInterface.getInstance();

        /* Allowing web content debugging. */
        if (0 != (getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                WebView.setWebContentsDebuggingEnabled(true);
            }
        }

        WebSettings webSettings = this.myWebView.getSettings(); /* Initializing webSettings for myWebView object. */
        webSettings.setJavaScriptEnabled(true); /* Enabling Javascript. */
        this.myWebView.addJavascriptInterface(appInterface, "Android");
        webSettings.setDomStorageEnabled(true); /* Enabling Local Storage. */
        webSettings.setAppCacheEnabled(false);

        this.myWebView.setWebChromeClient(webChromeClient); /* Preferred for animations like Progress Bar, etc. */
        this.myWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                if (showLoader) {
                    pBar.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                pBar.setVisibility(View.GONE);
                showLoader = false;
            }

            @Override
            public boolean shouldOverrideUrlLoading(final WebView view, String url) {
                if (url.equals(Constants.CHECKOUT_SUCCESS_URL)) {
                    appInterface.setListener(new OrderDetail() {
                        @Override
                        public void getData(String orderSummary) {
                            view.post(new Runnable() {
                                @Override
                                public void run() {
                                    destroyWebView(view);
                                }
                            });
                        }
                    });
                }
                return false;
            }
        });

        try {
            postData = "name=" + URLEncoder.encode(first_name.trim(), "UTF-8")
                    + "&phone_no=" + URLEncoder.encode(phone_no.trim(), "UTF-8")
                    + "&email=" + URLEncoder.encode(email.trim(), "UTF-8")
                    + "&client_id=" + URLEncoder.encode(client_id.trim(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
//
        this.myWebView.postUrl(Constants.WEB_URL, postData.getBytes());
//        this.myWebView.loadUrl(Constants.WEB_URL);
    }

    private void destroyWebView(WebView view) {
        finish();
        webChromeClient.onCloseWindow(myWebView);
        view.removeAllViews();
        view.destroyDrawingCache();
        // NOTE: clears RAM cache, if you pass true, it will also clear the disk cache.
        // Probably not a great idea to pass true if you have other WebViews still alive.
        view.clearCache(false);
        view.destroy();
        myWebView = null;   // Null out the reference so that you don't end up re-using it.
    }

    public void onBackPressed() {
        if (this.myWebView.canGoBack()) {
            this.myWebView.goBack();
            return;
        }
        super.onBackPressed();
    }
}
