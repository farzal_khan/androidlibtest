package com.example.bitbucketlib;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.webkit.JavascriptInterface;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class WebAppInterface {

    private static WebAppInterface webAppInterface;
    private List<OrderDetail> lstOrderDetail = new ArrayList();

    private WebAppInterface() {
    }

    public static WebAppInterface getInstance() {
        if(webAppInterface == null) {
            webAppInterface = new WebAppInterface();
        }
        return webAppInterface;
    }

    public void setListener(OrderDetail orderDetail) {
        lstOrderDetail.add(orderDetail);
    }

    @JavascriptInterface
    public String showToast(String orderDetails) {
        try {
            JSONObject jsonData = new JSONObject(orderDetails);
            Log.d("RESPONSE_RECEIVED", orderDetails.toString());
            for(OrderDetail orderDetail : lstOrderDetail) {
                if(orderDetail != null) {
                    orderDetail.getData(orderDetails.toString());
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "";
    }
}
