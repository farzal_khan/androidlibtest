package com.example.bitbucketlib;

public class Constants {
    public static final String CHECKOUT_SUCCESS_URL = "https://customapp.yayvo.com/shop/checkout/success";
    public static final String WEB_URL = "https://customapp.yayvo.com/shop/api/register-login";
}
